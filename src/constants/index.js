import * as Colors from './colors';
import * as Routes from './routes';

export {
  Colors,
  Routes,
};
